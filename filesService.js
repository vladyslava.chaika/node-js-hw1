// requires...
const fs = require('fs');
const path = require('path')

// constants...
const directory = './files/';

function createFile (req, res, next) {
  const content = req.body.content
  const filename = req.body.filename
  if (!filename) {
      res.status(400).send({message: "Please specify 'filename' parameter"});
  } else if (!content) {
      res.status(400).send({ message: "Please specify 'content' parameter" });
  } else {
    fs.writeFileSync(`${directory}${filename}`, content, 'utf-8');
      res.status(200).send({ message: 'File created successfully' });
  }
}

function getFiles (req, res, next) {
  fs.readdir(directory, (err, files) => {
    if( err ) {
      next({message: 'Directory is empty', status: 500})
      return;
    }
    res.status(200).send({
      "message": "Success",
      "files": files});
  });
}

const getFile = (req, res, next) => {
  const filename = req.params.filename;
  try {
    const ext = path.extname(filename).substring(1);
    const filePath = path.join(directory, filename);
    const content = fs.readFileSync(filePath, 'utf-8');
    const creationDate = fs.statSync(filePath).birthtime;

    res.status(200).send({
      "message": "Success",
      "filename": filename,
      "content":  content,
      "extension": ext,
      "uploadedDate": creationDate
    });
  }catch (err){
    res.status(400).send({'message': `No file with '${filename}' found`});
  }
};


module.exports = {
  createFile,
  getFiles,
  getFile
}
